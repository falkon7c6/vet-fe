const articles = document.getElementById("articles");

function createName(article, nameText) {
    const name = document.createElement("h3");
    name.innerText = nameText;
    article.appendChild(name);
}

function createImage(article, imgUrl) {
    const img = document.createElement("img");
    img.src = imgUrl;
    article.appendChild(img);
}

function createDeleteButton(article) {
    createButton(article, "Eliminar", function() {
        articles.removeChild(this.parentElement); 
    }, "deleteButton");
}

function createProfileButton(article) {
    createButton(article, "Ver Perfil", function() {
        alert("perfil");
    }, "profileButton");
}

function createButton(article, text, listenerFunction, className) {
    const button = document.createElement("button");
    button.innerHTML = text;
    button.addEventListener('click', listenerFunction);
    button.classList.add(className);
    article.appendChild(button);
}

function createDoctors() {
    createDoctor("Dr Elba Gallo", "https://papelmatic.com/wp-content/uploads/2019/09/papelmatic-higiene-profesional-limpieza-desinfeccion-clinicas-veterinarias-1080x675.jpg")
    createDoctor("Dra Ana Liza Meltrozo" , "https://papelmatic.com/wp-content/uploads/2019/09/papelmatic-higiene-profesional-limpieza-desinfeccion-clinicas-veterinarias-1080x675.jpg");
}

function createDoctor(name, imgUrl) {
    const article = document.createElement("article");
    createName(article, name);
    createImage(article, imgUrl);
    createDeleteButton(article);
    createProfileButton(article);
    articles.appendChild(article);
}

createDoctors();